package io.udl.test;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import io.udl.UDL;
import io.udl.annotation.DeepLink;

@DeepLink("/content")
public class ContentActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content);

        if (UDL.isFromDeepLink(this)) {
            Bundle data = getIntent().getBundleExtra("udl");
            TextView text = (TextView) findViewById(R.id.text_received_data);
            text.setText("Received data : " + data.getString("data", "NONE"));
        }
    }

    public void onGoalClicked(View view) {
        Toast.makeText(this, "Reached our goal!", Toast.LENGTH_SHORT).show();
    }
}
