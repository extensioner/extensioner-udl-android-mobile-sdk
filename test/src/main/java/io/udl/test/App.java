package io.udl.test;

import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import io.udl.DeepLinkRouter;
import io.udl.UDL;

public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        UDL.init(this, "APP_ID_HERE");
        UDL.setRouter(new DeepLinkRouter() {
            @Override
            public void onLink(String url, Bundle data, Context context) {
                Log.e("UDLTest", "Received : " + url);
                Toast.makeText(context, "Router has received " + url, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
