package io.udl;

import android.content.Context;
import android.os.Bundle;

/**
 * Copyright (C) 2015 Teheran Slippers. All rights are reserved.
 */
public interface DeepLinkRouter {
    void onLink(String url, Bundle data, Context context);
}
