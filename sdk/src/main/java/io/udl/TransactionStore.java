package io.udl;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

/**
 * Saves transaction ID.
 */
public class TransactionStore {

    private static String transactionId;
    private static SharedPreferences prefs;

    static void initAndLoad(Context context) {
        prefs = context.getSharedPreferences("udl_transaction", Context.MODE_PRIVATE);
        transactionId = prefs.getString("transactionId", null);

        if (transactionId != null) Log.d("UDL", "TransactionId Loaded : " + transactionId);
    }

    static void setTransactionId(String id) {
        transactionId = id;
        prefs.edit().putString("transactionId", id).apply();
    }

    public static String getTransactionId() {
        return transactionId;
    }

    static void clearTransactionId() {
        prefs.edit().clear().apply();
        transactionId = null;
    }
}
