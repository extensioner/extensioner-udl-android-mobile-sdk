package io.udl;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * 서버 API 호출 시 보낼 파라미터를 작성할 때 사용된다.
 * Copyright (C) 2015 Teheran Slippers. All rights are reserved.
 *
 * @author vista
 */
public class Param {
    private HashMap<String, Object> paramMap;

    public Param() {
        paramMap = new HashMap<String, Object>();
    }

    public Param put(String key, Object value) {
        paramMap.put(key, value);
        return this;
    }

    public Param put(String key, Param value) {
        paramMap.put(key, value.toJson());
        return this;
    }

    public Param put(String key, List value) {
        put(key, new JSONArray(value));
        return this;
    }

    public Object get(String key) {
        return paramMap.get(key);
    }

    /**
     * 파라미터들을 URL Encoding된 포맷으로 변환한다. (param1=name&param2=name)
     * @return String (URL Encoded UTF-8)
     */
    String toUrl() {
        try {
            StringBuilder query = new StringBuilder();
            Iterator iter = paramMap.entrySet().iterator();
            while (iter.hasNext()) {
                Map.Entry entry = (Map.Entry) iter.next();
                query.append(entry.getKey() + "="
                        + URLEncoder.encode((String) entry.getValue(), "utf-8") + "&");
            }
            return query.toString();

        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    /**
     * 파라미터들을 JSON으로 변환한다.
     * @return JSONObject instance
     */
    public JSONObject toJson() {
        return new JSONObject(paramMap);
    }
}
