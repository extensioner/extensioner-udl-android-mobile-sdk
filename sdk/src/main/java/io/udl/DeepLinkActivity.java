package io.udl;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

/**
 * An dummy activity that Handles deep link from udl.io.
 */
public class DeepLinkActivity extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // get link
        Intent intent = getIntent();
        if (intent == null) {
            finish();
            return;
        }

        Uri uri = intent.getData();
        if (uri == null || !uri.getBooleanQueryParameter("udl", false)) {
            // not from UDL!
            finish();
            return;
        }

        LinkHandler.handleUri(uri, this);
        finish();
    }
}
