package io.udl;

import android.content.ComponentCallbacks2;
import android.content.res.Configuration;

/**
 *
 */
public class BackgroundDetector implements ComponentCallbacks2 {
    @Override
    public void onTrimMemory(int level) {
        if (TransactionStore.getTransactionId() != null && level == TRIM_MEMORY_UI_HIDDEN) {
            Api.background();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {

    }

    @Override
    public void onLowMemory() {

    }
}
