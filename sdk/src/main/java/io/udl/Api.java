package io.udl;

import android.os.Build;

import io.udl.networking.RequestQueue;
import io.udl.networking.UDLRequest;

/**
 * udl.io API.
 */
class Api {

    private static RequestQueue queue;

    static void init() {
        queue = RequestQueue.getInstance();
    }

    static void linkClicked() {
        Param clientData = new Param();
        clientData.put("deviceType", Build.MODEL);
        clientData.put("mobileUUID", UDL.getDeviceUuid());
        clientData.put("appVersion", UDL.getAppVersion());
        clientData.put("osVersion", "Android" + Build.VERSION.RELEASE);
        clientData.put("screenSize", UDL.getScreenSize());

        queue.enqueue(new UDLRequest()
                .setParameter(new Param()
                                .put("eventCategory", 101)
                                .put("eventType", 0)
                                .put("clientData", clientData)
                                .put("additionalData", new Param())
                ));
    }

    static void goal(int eventType, String goalKey, String goalCategory, String goalLabel) {
        Param clientData = new Param();
        clientData.put("mobileUUID", UDL.getDeviceUuid());

        Param additionalData = new Param();
        clientData.put("goalKey", goalKey);
        clientData.put("goalCategory", goalCategory);
        clientData.put("goalLabel", goalLabel);

        queue.enqueue(new UDLRequest()
                .setParameter(new Param()
                                .put("eventCategory", 102)
                                .put("eventType", eventType)
                                .put("clientData", clientData)
                                .put("additionalData", additionalData)
                ));
    }

    static void background() {
        Param clientData = new Param();
        clientData.put("mobileUUID", UDL.getDeviceUuid());

        queue.enqueue(new UDLRequest()
                .setParameter(new Param()
                                .put("eventCategory", 103)
                                .put("eventType", 0)
                                .put("clientData", clientData)
                                .put("additionalData", new Param())
                ));
    }

    static void deferredCheck() {
        Param clientData = new Param();
        clientData.put("mobileUUID", UDL.getDeviceUuid());

        queue.enqueue(new UDLRequest()
                .setParameter(new Param()
                                .put("eventCategory", 103)
                                .put("eventType", 0)
                                .put("clientData", clientData)
                                .put("additionalData", new Param())
                ));
    }

    static void view(int id, String name, String info) {
        Param clientData = new Param();
        clientData.put("mobileUUID", UDL.getDeviceUuid());
        clientData.put("name", name);
        clientData.put("info", info);

        queue.enqueue(new UDLRequest()
                .setParameter(new Param()
                                .put("eventCategory", 105)
                                .put("eventType", id)
                                .put("clientData", clientData)
                                .put("additionalData", new Param())
                ));
    }
}
