package io.udl;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import java.net.URLDecoder;

public class DeferredLinkReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            String referrer = intent.getExtras().getString("referrer");
            Uri uri = Uri.parse(URLDecoder.decode(referrer, "UTF-8"));

            Log.d("UDL", "Deferred deeplink : " + uri.toString());
            LinkHandler.handleUri(uri, context);

        } catch (Exception e) {
            Log.e("UDL", e.getMessage());
        }
    }
}
