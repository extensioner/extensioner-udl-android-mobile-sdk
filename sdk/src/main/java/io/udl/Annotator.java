package io.udl;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.util.Log;

import java.util.HashMap;
import java.util.Map;

import io.udl.annotation.DeepLink;

/**
 * Processes all annotation in activities - in runtime.
 */
class Annotator {
    static Map<String, Class> routeMap = new HashMap<>();

    /**
     * Scan all the activities and make a route map.
     * @param context App Context
     */
    public static void makeRouteMap(Context context) {
        try {
            ActivityInfo infos[] = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), PackageManager.GET_ACTIVITIES)
                    .activities;

            for (ActivityInfo info : infos) {
                Class activityClass = Class.forName(info.name);
                DeepLink annotation = (DeepLink) activityClass.getAnnotation(DeepLink.class);
                if (annotation != null && annotation.value() != null) {
                    routeMap.put(annotation.value(), activityClass);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
