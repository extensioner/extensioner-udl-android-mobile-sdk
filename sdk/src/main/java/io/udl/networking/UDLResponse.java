package io.udl.networking;

/**
 * TODO: Further use (Error handling, etc...)
 */
public class UDLResponse {
    public int status;
    public String body;

    public UDLResponse(int status, String body) {
        this.status = status;
        this.body = body;
    }
}
