package io.udl.networking;

/**
 *
 */
interface UDLClient {
    UDLResponse call(UDLRequest request) throws Exception;
}
