package io.udl.networking;

import android.content.Context;

import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.conn.params.ConnPerRouteBean;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;

/**
 *
 */
@SuppressWarnings("deprecation")
class ApacheHttpClient implements UDLClient {
    private HttpClient client;

    public ApacheHttpClient(Context context) {
        HttpParams params = new BasicHttpParams();

        HttpConnectionParams.setStaleCheckingEnabled(params, false);

        HttpConnectionParams.setConnectionTimeout(params, 10000);
        HttpConnectionParams.setSoTimeout(params, 10000);
        HttpConnectionParams.setSocketBufferSize(params, 8192);
        HttpClientParams.setRedirecting(params, false);

        ConnManagerParams.setMaxConnectionsPerRoute(params, new ConnPerRouteBean(20));
        ConnManagerParams.setMaxTotalConnections(params, 20);

        String host = System.getProperty("http.proxyHost");
        String portString = System.getProperty("http.proxyPort");
        if ((host != null) && (host.length() != 0) && (portString != null) && (portString.length() != 0)) {
            int port = Integer.parseInt(portString);
            HttpHost proxy = new HttpHost(host, port, "http");
            params.setParameter("http.route.default-proxy", proxy);
        }

        client = new DefaultHttpClient(new ThreadSafeClientConnManager(params,
                new DefaultHttpClient().getConnectionManager().getSchemeRegistry()), params);
    }

    @Override
    public UDLResponse call(UDLRequest request) throws Exception {
        HttpUriRequest req;

        // Write HTTP Request
        switch(request.getMethod()) {
            case "GET": // GET
                String url = request.getUrl();
                req = new HttpGet(url);
                break;

            case "POST":
                req = new HttpPost(request.getUrl());
                ((HttpPost) req).setEntity(new StringEntity(request.getParam(), "utf-8"));
                break;

            case "PUT":
                req = new HttpPut(request.getUrl());
                ((HttpPut) req).setEntity(new StringEntity(request.getParam(), "utf-8"));
                break;

            case "DELETE":
                req = new HttpDelete(request.getUrl());
                break;

            default: // can't be possible!
                throw new RuntimeException("method " + request.getMethod() + " does not exist!");
        }

        if (!(req instanceof HttpGet)) {
            // Sending Standard JSON Requests
            req.setHeader("Accept", "application/json");
            req.setHeader("Accept-Encoding", "utf-8");
            req.setHeader("Content-Type", "application/json");
        }

        HttpResponse response = client.execute(req);

        // Read the response
        String body = EntityUtils.toString(response.getEntity(), "utf-8");

        return new UDLResponse(response.getStatusLine().getStatusCode(), body);
    }
}
