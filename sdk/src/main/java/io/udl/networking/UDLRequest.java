package io.udl.networking;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

import io.udl.Param;
import io.udl.TransactionStore;
import io.udl.UDL;

/**
 * UDL API 서버에 비동기적 API 호출을 하기 위해서 사용된다.
 * Copyright (C) 2015 Teheran Slippers. All rights are reserved.
 */
public class UDLRequest {
    private static String ENDPOINT = "http://stat.udl.io:5000/stat/mobile/";

    private static final List<String> ALLOWED_METHODS =
            Arrays.asList("GET", "POST", "PUT", "DELETE");

    private static String appId, appVer;
    private static UDLClient client;

    private String url, method;
    private JSONObject param;

    /**
     * UDLRequest를 초기화시킨다.
     */
    public static void init(Context context) {
        if (client == null) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.GINGERBREAD) {
                client = new ApacheHttpClient(context);
            } else {
                client = new URLConnectionClient(context);
            }
        }
        appId = UDL.getAppId();
        appVer = UDL.getVersion();
        ENDPOINT += UDL.getAppId();
    }

    public UDLRequest() {
        method = "POST";
        url = ENDPOINT;
        Param defaultParam = new Param();
        defaultParam
                .put("checkFunctionality", 0)
                .put("uuid", appId)
                .put("sdkVersion", appVer)
                .put("transactionId", TransactionStore.getTransactionId())
                .put("originUrl", 0);
    }

    /**
     * API 콜을 호출한다.
     */
    public void callAsync() {
        new AsyncTask<UDLRequest, Void, Void>() {
            @Override
            protected Void doInBackground(UDLRequest... params) {
                UDLRequest request = params[0];
                try {
                    UDLResponse response = client.call(request);

                } catch (Exception e) {
                    Log.e("UDL", request.getMethod() + " " + request.getUrl());
                }
                return null;
            }
        }.execute(this);
    }

    /**
     * Request Body에 실어보낼 파라미터를 지정한다.
     * @param param Param
     */
    public UDLRequest setParameter(Param param) {
        this.param = param.toJson();
        return this;
    }

    /**
     * HTTP 메서드를 설정한다. 기본값은 GET이다.
     * @param method  메서드
     */
    public void setMethod(String method) {
        if (!ALLOWED_METHODS.contains(method)) {
            throw new IllegalArgumentException("method " + method + " does not exist!");
        }
        this.method = method;
    }

    /**
     * @return HTTP method
     */
    public String getMethod() {
        return method;
    }

    /**
     * @return URL
     */
    public String getUrl() {
        return url;
    }

    /**
     * @return parameters (JSON)
     */
    public String getParam() {
        return param.toString();
    }
}
