package io.udl.networking;

import android.content.Context;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 *
 */
class URLConnectionClient implements UDLClient {

    public URLConnectionClient(Context context) {

    }

    @Override
    public UDLResponse call(UDLRequest request) throws Exception {
        HttpURLConnection connection = null;

        try {
            URL url = new URL(request.getUrl());
            connection = (HttpURLConnection) url.openConnection();

            connection.setRequestMethod(request.getMethod());
            if (!request.getMethod().equals("GET")) {
                connection.setDoOutput(true);
                connection.setChunkedStreamingMode(0);

                OutputStream out = new BufferedOutputStream(connection.getOutputStream());
                out.write(request.getParam().getBytes("utf-8"));
                out.flush();
                out.close();
            }

            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String line;
            StringBuilder body = new StringBuilder();
            while ((line = in.readLine()) != null) body.append(line);
            in.close();

            return new UDLResponse(connection.getResponseCode(), body.toString());

        } finally {
            if (connection != null) connection.disconnect();
        }
    }
}
