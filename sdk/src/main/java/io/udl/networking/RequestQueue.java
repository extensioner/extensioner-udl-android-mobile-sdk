package io.udl.networking;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

import io.udl.UDL;

/**
 * Request를 오프라인에 큐잉하고 네트워크가 연결되면 다시 보낸다.
 */
public class RequestQueue {
    private static RequestQueue instance;

    public static RequestQueue getInstance() {
        if (instance == null) instance = new RequestQueue();
        return instance;
    }

    private Context context;
    private SharedPreferences prefs;
    private Queue<UDLRequest> queue = new LinkedBlockingQueue<>();
    private IntentFilter reconnectIntent = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
    private ConnectivityManager cm;

    private RequestQueue() {
        context = UDL.getContext();
        prefs = context.getSharedPreferences("request_queue", Context.MODE_PRIVATE);
        cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
    }

    /**
     * Enqueue the request if the network state is offline.
     * @param request {UDLRequest} that you want to sent
     */
    public void enqueue(UDLRequest request) {
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork.isConnectedOrConnecting();

        if (!isConnected) {
            queue.add(request);
            context.registerReceiver(reconnectReceiver, reconnectIntent);
        }
        else request.callAsync();
    }

    /**
     * Called when network gets back online.
     */
    private BroadcastReceiver reconnectReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context c, Intent intent) {
            // send all messages
            UDLRequest request;
            while ((request = queue.poll()) != null) request.callAsync();

            // clear offline queue
            prefs.edit().clear().apply();

            context.unregisterReceiver(this);
        }
    };

}
