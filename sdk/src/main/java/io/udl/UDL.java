package io.udl;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.graphics.Point;
import android.telephony.TelephonyManager;
import android.view.Display;
import android.view.WindowManager;

import java.net.URI;
import java.util.UUID;

import io.udl.networking.UDLRequest;

/**
 * Uniform DeepLink SDK for Android.
 * Copyright (C) 2015 Teheran Slippers. All rights are reserved.
 *
 * @author vista
 */
public class UDL {
    private static final String VERSION = "1.0";

    private static String appId, uuid;
    private static Context context;

    private static DeepLinkRouter router;

    /**
     * SDK를 초기화한다.
     * Application의 onCreate 메서드 혹은 맨 처음 Activity에서 반드시 한 번 호출되어야 한다.
     *
     * @param context App Context
     * @param appId Application ID (udl.io에서 발급받은)
     */
    public static void init(Context context, String appId) {
        if (appId == null) throw new IllegalArgumentException("App ID was not given!");
        UDL.context = context.getApplicationContext();
        UDL.appId = appId;

        Api.init();
        UDLRequest.init(context);
        Annotator.makeRouteMap(context);
        TransactionStore.initAndLoad(context);

        // detect on app goes to background
        context.registerComponentCallbacks(new BackgroundDetector());
    }

    /**
     * 링크가 전송될 시 호출될 Router를 설정한다. {@link io.udl.DeepLinkRouter}
     * Router에서는 URL (path)에 알맞은 Activity를 호출하거나, 특정 동작을 실행해야 한다.
     *
     * @param router Router
     */
    public static void setRouter(DeepLinkRouter router) {
        UDL.router = router;
    }

    /**
     * 이 액티비티가 Deep Link를 타고 열렸는지 체크한다.
     * {@link io.udl.annotation.DeepLink} 어노테이션을 액티비티에 붙였을 때만 사용 가능하다.
     *
     * @param activity Activity
     * @return Deep Link를 통해 열렸는지 여부
     */
    public static boolean isFromDeepLink(Activity activity) {
        return activity.getIntent() != null && activity.getIntent().hasExtra("udl");
    }


    public static void goal(String key, String category, String data) {
        Api.goal(1, key, category, data);
        TransactionStore.clearTransactionId();
    }

    public static Context getContext() {
        return context;
    }

    /**
     * @return UDL SDK Version
     */
    public static String getVersion() {
        return "M_A_v" + VERSION;
    }

    public static String getAppId() {
        return appId;
    }

    // ===== Private APIs =====

    static DeepLinkRouter getRouter() {
        return UDL.router;
    }

    static String getDeviceUuid() {
        if (uuid == null) {
            TelephonyManager tm =
                    (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

            String tmDevice, tmSerial, androidId;
            tmDevice = "" + tm.getDeviceId();
            tmSerial = "" + tm.getSimSerialNumber();
            androidId = "" +
                    android.provider.Settings.Secure.getString(context.getContentResolver(),
                            android.provider.Settings.Secure.ANDROID_ID);

            UUID deviceUuid = new UUID(androidId.hashCode(),
                    ((long)tmDevice.hashCode() << 32) | tmSerial.hashCode());
            uuid = deviceUuid.toString();
        }
        return uuid;
    }

    static String getScreenSize() {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return String.format("%dx%d", size.x, size.y);
    }

    static String getAppVersion() {
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return pInfo.versionName;
        } catch (Exception e) {
            return "null";
        }
    }
}
