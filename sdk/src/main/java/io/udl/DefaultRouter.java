package io.udl;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

/**
 *
 */
public class DefaultRouter implements DeepLinkRouter {
    @Override
    public void onLink(String url, Bundle data, Context context) {
        if (Annotator.routeMap.containsKey(url)) {
            Intent intent = new Intent(context, Annotator.routeMap.get(url));
            intent.putExtra("udl", data);
            context.startActivity(intent);
        }
    }
}
