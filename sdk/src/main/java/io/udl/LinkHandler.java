package io.udl;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

/**
 *
 */
public class LinkHandler {
    private static DeepLinkRouter generatedRouter;
    private static boolean isFirst = true;

    static void handleUri(Uri uri, Context context) {
        // set transaction id and link event
        String transactionId = uri.getQueryParameter("transactionId");
        TransactionStore.setTransactionId(transactionId);

        // make bundle
        Bundle data = new Bundle();
        for (String key : uri.getQueryParameterNames()) {
            if (key.equals("transactionId") || key.equals("udl")) continue;
            data.putString(key, uri.getQueryParameter(key));
        }

        // look for the generated router (DefaultRouter)
        // TODO: Remove and use DefaultRouter
        if (isFirst) {
            try {
                Class routerClass = Class.forName("io.udl.DefaultRouter");
                generatedRouter = (DeepLinkRouter) routerClass.newInstance();

            } catch (ClassNotFoundException e) {
                Log.d("UDL", "DefaultRouter is not found. continuing...");

            } catch (Exception e) {
                Log.e("UDL", "DefaultRouter initialization failed : " + e.getMessage(), e);
            }
            isFirst = false;
        }

        // run generated router
        if (generatedRouter != null) generatedRouter.onLink(uri.getPath(), data, context);

        // run user set router
        if (UDL.getRouter() != null) UDL.getRouter().onLink(uri.getPath(), data, context);
    }
}
