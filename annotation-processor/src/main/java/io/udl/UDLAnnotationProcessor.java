package io.udl;

import com.google.auto.service.AutoService;
import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.TypeSpec;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Filer;
import javax.annotation.processing.Messager;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.Processor;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import javax.tools.Diagnostic;

import io.udl.annotation.DeepLink;

@AutoService(Processor.class)
public class UDLAnnotationProcessor extends AbstractProcessor {

    private Types typeUtils;
    private Elements elementUtils;
    private Filer filer;
    private Messager messager;

    @Override
    public synchronized void init(ProcessingEnvironment processingEnv) {
        super.init(processingEnv);
        typeUtils = processingEnv.getTypeUtils();
        elementUtils = processingEnv.getElementUtils();
        messager = processingEnv.getMessager();
        filer = processingEnv.getFiler();
    }

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        List<RouteAnnotatedElement> routeList = new ArrayList<>();
        for (Element element : roundEnv.getElementsAnnotatedWith(DeepLink.class)) {
            if (!isValidClass((TypeElement) element)) return true; // error printed, exit
            routeList.add(new RouteAnnotatedElement(element));
        }

        // generate new class file
        if (!routeList.isEmpty()) {
            try {
                writeClassFile(routeList);
            } catch (IOException e) {
                messager.printMessage(Diagnostic.Kind.ERROR, "Error creating file");
            }
        }
        return false;
    }

    private boolean isValidClass(TypeElement element) {
        if (element.getKind() == ElementKind.CLASS) {
            error(element, "Only classes can be annotated with @DeepLink");
            return false;
        }

//        // check superclass
//        TypeElement currentClass = element;
//        while (true) {
//            TypeMirror superClass = currentClass.getSuperclass();
//            if (superClass.getKind() == TypeKind.NONE) {
//                // java.lang.Object reached
//                error(element, "Class %s must be an Activity.", element.getSimpleName());
//                return false;
//            }
//            else if (superClass.toString().equals("android.app.Activity")) break;
//
//            currentClass = (TypeElement) typeUtils.asElement(superClass);
//        }

        return true;
    }

    private void writeClassFile(List<RouteAnnotatedElement> routeList) throws IOException {

        // @Override public void onLink(String path, Bundle data) { ... }
        MethodSpec.Builder builder = MethodSpec.methodBuilder("onLink")
                .addAnnotation(Override.class)
                .addModifiers(Modifier.PUBLIC)
                .returns(void.class)
                .addParameter(String.class, "path")
                .addParameter(ClassName.get("android.os", "Bundle"), "data")
                .addParameter(ClassName.get("android.content", "Context"), "context")
                .beginControlFlow("switch (path) {");

        for (RouteAnnotatedElement element : routeList) {
            builder.addStatement("case $S:", element.path)
                    .addStatement("$T intent = new Intent(context, $T.class)",
                            ClassName.get("android.content", "Intent"), element.activity)
                    .addStatement("intent.putExtra($S, data)", "udl")
                    .addStatement("context.startActivity(intent)")
                    .addStatement("return");
        }
        MethodSpec onLink = builder.endControlFlow().build();

        // public DeepRouter implements DeepLinkRouter { ... }
        TypeSpec router = TypeSpec.classBuilder("DeepRouter")
                .addModifiers(Modifier.PUBLIC)
                .addSuperinterface(ClassName.get("io.udl", "DeepLinkRouter"))
                .addMethod(onLink)
                .build();

        // create java file
        JavaFile.builder("io.udl", router)
                .build()
                .writeTo(filer);
    }

    private void error(Element e, String msg, Object... args) {
        messager.printMessage(Diagnostic.Kind.ERROR, String.format(msg, args), e);
    }

    @Override
    public Set<String> getSupportedAnnotationTypes() {
        return super.getSupportedAnnotationTypes();
    }

    @Override
    public SourceVersion getSupportedSourceVersion() {
        return SourceVersion.latestSupported();
    }
}
