package io.udl;

import javax.lang.model.element.Element;

import io.udl.annotation.DeepLink;

/**
 * POJO
 */
public class RouteAnnotatedElement {
    public String path, activity;

    public RouteAnnotatedElement(Element element) {
        DeepLink annotation = element.getAnnotation(DeepLink.class);
        path = annotation.value();
        activity = element.toString();
    }
}
