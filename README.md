[UDL.io](http://udl.io) Android SDK
================

This is repository of [UDL.io](http://plugy.io) Mobile Deep-Linking SDK for Android.  
You can handle deep links and analyze statics using this SDK.

## Installation
Add following line to the 'dependencies' block in `app/build.gradle` file :
```groovy
  compile 'io.udl:sdk-android:1.0.0'
```

## Usage
1) Add `@DeepLink` annotation to the top of the `Activity` you want to handle deep links.
```java
@DeepLink("/video/show")
public class VideoActivity {
   ...
```

2) Add following code to check whether the intent is deep link or not.
```java
    @Override
    public void onCreate(Bundle savedState) {
        super.onCreate(savedState);
        setContentView(R.layout.some_layout);

        if (UDL.isFromDeepLink(this)) {
            Bundle data = getIntent().getBundleExtra("udl");
            // TODO: handle deep link!
        }
    }
```
